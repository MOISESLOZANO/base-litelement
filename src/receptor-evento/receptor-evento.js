import { LitElement, html } from 'lit-element';

class ReceptorEvento extends LitElement{
        
        static get properties(){
                return {
                        course: {type: String},
                        year: {type: String}
                };
        }

    render() {
        return html` 
                <h1>Receptor Evento</h1>
                <h5>Curso es de ${this.course}</h5>
                <h5>estamos en el año${this.year}</h5>
        `;
        } 

}

customElements.define('receptor-evento', ReceptorEvento);