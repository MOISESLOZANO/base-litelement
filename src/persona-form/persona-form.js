import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement{

        static get properties() {
                return {
                        person: {type: Object},
                        editingPerson: {type: Boolean}
                }
        }

        constructor() {
                super();

                this.resetFormData();
        }


    render() {
        return html` 
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
                <div>
                        <form>
                                <div class="form-group">
                                        <label>Nombre Completo</label>
                                        <input type="text" id="personaFormName" class="form-control" placeholder="Nombre" 
                                        @input="${this.udpateName}"
                                        .value="${this.person.name}"
                                        ?disabled="${this.editingPerson}"
                                        />
                                </div>
                                <div class="form-group">
                                        <label>Descripcion</label>
                                        <textarea class="form-control" placeholder="Descripcion" rows="5"
                                        @input="${this.udpateProfile}"
                                        .value="${this.person.profile}"></textarea>
                                </div>
                                <div class="form-group">
                                        <label>Años de crecimiento</label>
                                        <input type="text" class="form-control" placeholder="Años de crecimiento" 
                                        @input="${this.udpateYearsInCompany}"
                                        .value="${this.person.yearsInCompany}"/>
                                </div>
                                <button class="btn btn-primary" @click="${this.goBack}"><strong>Atras</strong></button>
                                <button class="btn btn-success" @click="${this.storePerson}"><strong>Guardar</strong></button>
                        </form>
                </div>
        `;
        }

        udpateName(e){
                console.log("udpateName");
                console.log("actualizando la propiedad name con el valor " + e.target.value);
                this.person.name = e.target.value;
        }

        udpateProfile(e){
                console.log("udpateProfile");
                console.log("actualizando la propiedad profile con el valor " + e.target.value);
                this.person.profile = e.target.value;
        }

        udpateYearsInCompany(e){
                console.log("udpateYearsInCompany");
                console.log("actualizando la propiedad YearsInCompany con el valor " + e.target.value);
                this.person.yearsInCompany = e.target.value;
        }

        goBack(e) {
                console.log("goBack");
                e.preventDefault();

                this.dispatchEvent( new CustomEvent("persona-form-close", {}) );

                this.resetFormData();
                console.log("resetFormData2");
        }

        storePerson(e) {
                console.log("storePerson");
                e.preventDefault();

                this.person.photo = {
                        src: "./img/persona.jpg",
                        alt: "Persona"
                };

                console.log("La propiedad name: " + this.person.name);
                console.log("La propiedad profile: " + this.person.profile);
                console.log("La propiedad yearsInCompany: " + this.person.yearsInCompany);

                this.dispatchEvent(new CustomEvent("persona-form-store", {
                        detail: {
                                person: {
                                        name: this.person.name,
                                        profile: this.person.profile,
                                        yearsInCompany: this.person.yearsInCompany,
                                        photo: this.person.photo
                                },
                                editingPerson: this.editingPerson
                        }
                }
                ));
        }

        resetFormData() {
                console.log("resetFormData1");
                this.person = {};
                this.person.name = "";
                this.person.yearsInCompany = "";
                this.person.profile = "";

                console.log("editingPerson");
                this.editingPerson = false;
        }
}

customElements.define('persona-form', PersonaForm);