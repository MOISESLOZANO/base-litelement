import { LitElement, html } from 'lit-element';

class PersonaMainDM extends LitElement{
        static get properties() {
                return {
                        people: {type: Array}
                };
        }

        constructor() {
                super();

                this.people = [
                        { 
                        name: "Pera",
                        yearsInCompany: 10,
                        profile: "lorem ipsum 1",
                        photo: {
                                src: "./img/pera.jpg",
                                alt: "Pera"
                                }
                        },
                        { 
                        name: "Manzana", 
                        yearsInCompany: 2,
                        profile: "lorem ipsum 2",
                        photo: {
                                src: "./img/manzana.jpg",
                                alt: "Manzana"
                                }
                        },
                        { 
                        name: "Naranja",
                        yearsInCompany: 3,
                        profile: "lorem ipsum 3",
                        photo: {
                                src: "./img/naranja.jpg",
                                alt: "Naranja"
                                }
                        },
                        { 
                        name: "Kiwi",
                        yearsInCompany: 4,
                        profile: "lorem ipsum 4",
                        photo: {
                                src: "./img/kiwi.jpg",
                                alt: "Kiwi"
                                }
                        },
                        { 
                        name: "Uvas",
                        yearsInCompany: 5,
                        profile: "lorem ipsum 5",
                        photo: {
                                src: "./img/uvas.jpg",
                                alt: "Uvas"
                                }
                        }
                ];
        }

        updated(changedProperties){
                console.log("updated");
                if (changedProperties.has("people")){
                        console.log("Ha cambiado el valor de la propiedad people");
                        this.dispatchEvent(
                                new CustomEvent("people-data-updated", 
                                {
                                        detail: {
                                                people: this.people
                                        }
                                }
                        ));
                }
        }
}

customElements.define('persona-main-dm', PersonaMainDM);