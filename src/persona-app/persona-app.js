import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement {

        static get properties() {
                return {
                        people: {type: Array}
                };
        }

        render() {
                return html` 
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
                        <persona-header></persona-header>
                        <div class="row">
                                <persona-sidebar class="col-2" 
                                        @updated-max-years-filter="${this.newMaxYearsInCompanyFilter}"
                                        @new-person="${this.newPerson}"
                                >
                                </persona-sidebar>
                                <persona-main @updated-people="${this.updatedPeople}" class="col-10"></persona-main>
                        </div>
                        <persona-footer></persona-footer>
                        <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>
                `;
                }

        updated(changedProperties){
                console.log("updated en persona-app");

                if(changedProperties.has("people")){
                        console.log("Ha cambiado el valor de la propiedad people en persona-app");
                        console.log(this.people);
                        this.shadowRoot.querySelector("persona-stats").people = this.people;
                }
        }

        newMaxYearsInCompanyFilter(e){
                console.log("newMaxYearsInCompanyFilter");
                console.log("nuevo filtro es " + e.detail.maxYearsInCompany);
                this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.maxYearsInCompany;
        }

        updatedPeople(e) {
                console.log("updatedPeople en persona-app");
                console.log(e.detail.people);
                this.people = e.detail.people;
        }

        newPerson(e) {
                console.log("newPerson en persona-app");
                this.shadowRoot.querySelector("persona-main").showPersonForm = true;
        }

        updatedPeopleStats(e) {
                console.log("updatedPeopleStats en persona-app");
                this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
                this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.peopleStats.maxYearsInCompany;
        }
}

customElements.define('persona-app', PersonaApp);